<?php

namespace ApiSdk\TikTokShop\Servers;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use function Clue\StreamFilter\fun;
use function Sodium\compare;

class ProductService extends BaseService
{
    /**
     * Notes: 获取商品类目
     * author: lfy
     * Date: 2021/11/19
     * Time: 17:41
     * @throws GuzzleException
     */
    public function getCategory()
    {
        return $this->get('/api/products/categories');
    }

    /**
     * @Notes : 获取商品类目属性
     * @param $categoryId
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-04-07   15:05
     */
    public function getAttribute($categoryId)
    {
        $this->addApiParam('category_id', $categoryId);//类目id
        return $this->get('/api/products/attributes');
    }

    /**
     * @Notes : 获取商品类目 规则
     * @param $categoryId
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-04-07   15:06
     */
    public function getCategoryRule($categoryId)
    {
        $this->addApiParam('category_id', $categoryId);//类目id
        return $this->get('/api/products/categories/rules');
    }

    /**
     * Notes: 获取商品 品牌
     * author: lfy
     * Date: 2021/11/19
     * Time: 17:41
     * @throws GuzzleException
     */
    public function getBrand($page_number = 1, $page_size = 200, $categoryId = null, bool $only_authorized = null, $brand_suggest = null)
    {
        $this->addApiParam('page_size', $page_size);                                          //“page_size”代表返回列表分页，每页品牌数,[1,200] 之间的整数。“page_number”字段和“page_size”字段应该一起使用。page_number 字段和 page_size 字段的乘积不能超过 10,000。
        $this->addApiParam('page_number', $page_number);                                      // “页码”表示返回的品牌列表的页码。   从1开始的整数。“page_number”字段和“page_size”字段应该一起使用。page_number 字段和 page_size 字段的乘积不能超过 10,000。
        if (!empty($categoryId)) $this->addApiParam('category_id', $categoryId);              //类目id
        if (!empty($only_authorized)) $this->addApiParam('only_authorized', $only_authorized);//您可以使用“only_authorized”字段来决定是否只搜索授权品牌。
        if (!empty($brand_suggest)) $this->addApiParam('brand_suggest', $brand_suggest);      //您可以使用“brand_suggest”字段来搜索品牌名称。

        return $this->get('/api/products/brands');
    }

    /**
     * @Notes :上传图片
     * @param string $img_data 图像数据 输入图像格式文件。图片文件是base64编码生成的字符串。  图片文件，图片格式支持JPG、JPEG、PNG，图片像素至少600*600，最大20000*20000，原图最大尺寸：5MB
     * @param string $img_scene
     * 1:"PRODUCT_IMAGE" 横纵比例建议为1:1
     * 2:"DESCRIPTION_IMAGE"
     * 3:"ATTRIBUTE_IMAGE" 横纵比例建议为1:1
     * 4:"CERTIFICATION_IMAGE"
     * 5:"SIZE_CHART_IMAGE “
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-08   10:42
     */
    public function uploadImg(string $img_data, int $img_scene)
    {
        $this->addApiParam('img_data', $img_data);
        $this->addApiParam('img_scene', $img_scene);
        return $this->post('/api/products/upload_imgs');
    }

    /**
     * @Notes : 上传文件
     * @param string $file_data 文件数据 以 pdf 和 mp4 格式导入文件。文件是base64编码生成的字符串。如果文件类型为pdf，则原始文件大小不得超过10M。如果文件类型为mp4，原始文件大小不得超过20M，视频宽高比为9:16至16:9。
     * @param string|null $file_name 文件名
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-04-07   15:14
     */
    public function uploadFiles(string $file_data, string $file_name = '')
    {
        $this->addApiParam('file_data', $file_data);
        $this->addApiParam('file_name', $file_name);
        return $this->post('/api/products/upload_files');
    }

    /**
     * @Notes : 获取商品列表
     * @param int $page_number
     * @param int $page_size
     * @param int $search_status
     * @param int|null $update_time_from
     * @param int|null $update_time_to
     * @param int|null $create_time_from
     * @param int|null $create_time_to
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-04-18   19:39
     */
    public function getProductList($page_number = 1, int $page_size = 100, int $search_status = 0, int $update_time_from = null, int $update_time_to = null, int $create_time_from = null, int $create_time_to = null)
    {

        $this->addApiParam('page_number', $page_number ?? 1);
        $this->addApiParam('page_size', $page_size ?? 100);
        $this->addApiParam('search_status', !empty($search_status) ? $search_status : 0);//0-all、1-draft、2-pending、3-failed、4-live、5-seller_deactivated、6-platform_deactivated、7-freeze
        if (!empty($update_time_from)) $this->addApiParam('update_time_from', $update_time_from);
        if (!empty($update_time_to)) $this->addApiParam('update_time_to', $update_time_to);
        if (!empty($create_time_from)) $this->addApiParam('create_time_from', $create_time_from);
        if (!empty($create_time_to)) $this->addApiParam('create_time_to', $create_time_to);
        return $this->post('/api/products/search');

    }

    /**
     * @Notes : 获取商品详情
     * @param $product_id
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-04-18   20:27
     */
    public function getProductDetail($product_id)
    {
        $this->addApiParam('product_id', $product_id);//商品id
        return $this->get('/api/products/details');
    }

    /**
     * @Notes : 创建产品
     * @param string $product_name 产品名称 不允许使用中文字符。
     * 对于美国和英国商店，产品名称必须至少包含 1 个字符且不超过 255 个字符。其他地区的店铺，商品名称最少25个字符，最多255个字符。
     * @param string $description 描述 简要规则：
     *  1.必须符合html语法
     *  2.目前只支持<p> <img> <ul> <li>标签
     *  3.标签不能嵌套
     *  4.只能使用UploadImg接口的请求参数作为请求参数
     *  5. 最多30张图片（请通过UploadImg API转换img链接）。
     *  6.该字段字符限制需要在10000个字符以内。
     *  7. 建议避免使用中文，因为副本会显示给本地用户。
     *  8.以下丰富的格式不会导致listing产品错误但会在TikKTok Shop上丢失：粗体、下划线、斜体、字体、字体大小、字体颜色、文本对齐（居中/右/左）、 行高/字间距、序列号、Emoji
     *  因此，我们建议您在将产品同步到抖音商店后检查产品描述并
     *  进行必要的修改。
     * @param string $category_id 必须是叶子类别
     * @param array $images 图片 只能使用UploadImg API的请求参数作为请求参数   最多9张图片，上传顺序中的第一张图片会自动设置为头图
     *      ﹂  id
     * @param array $skus 一个产品的sku数量不能超过100个
     *      ﹂ sales_attributes      []     否   销售属性
     *         ﹂  attribute_id      细绳    否    属性编号 仅支持输入平台提供的销售属性ID    最多可输入三个销售属性
     *         ﹂  attribute_name    细绳    否    属性名称 字符长度不得超过20
     *         ﹂  value_id          细绳    否    值_id创建新的销售属性值时，不需要输入value_id。提交custom_value请求后，商户将获得平台分配的value_id（可在响应体中查看）
     *         ﹂  custom_value      细绳    否    自定义值 新建销售属性值时，要求商家输入自定义销售属性值，同一销售属性下不能有重复的销售属性值 建议避免使用中文，因为复制会显示给本地用户,字符长度不能超过50 每个销售属性下最多有100个属性值
     *         ﹂  sku_img           []     否    图片数量不得少于3张
     *            ﹂  ID             细绳    是     只能使用UploadImg接口的请求参数作为请求参数  最多只能为一个销售属性设置一张图片。当只有部分销售属性值设置了图片时，系统会使用商品头图片填写销售属性值，不带图片
     *      ﹂ stock_infos           []     否   库存信息
     *         ﹂  warehouse_id      细绳    是    仓库编号 该字段为必填字段（因为部分卖家会有多个仓库，卖家可以通过API GetWarehouseList获取仓库ID，创建SKU时必须选择一个仓库）。
     *         ﹂  available_stock   整数    是    可用库存取值必须为非负数（包括0）    一次设置的可用股票值上限为99999
     *      ﹂ seller_sku            细绳    否    卖家_sku 字符长度不得超过50 建议避免使用中文，因为文案会显示给本地用户
     *      ﹂ original_price        细绳    是     原价   印尼盾，最低100，最高1亿。印尼本地对本地业务，请注意，折扣价低于2000印尼盾的产品可能会导致负余额。
     *                                                  英国本地业务，最低0.01英镑，最高价格5600英镑
     *                                                  英国跨境业务，最低0.01英镑，最高134.5英镑
     *                                                  泰国本地业务，小数点后两位, 最低价30TNB, 最高价260000TNB
     *                                                  泰国跨境生意, 最低价0.01THB, 最高价260000THB
     *                                                  马来西亚本地企业(MYR), 最低价0.01 MRY, 最高价320000MYR
     *                                                  越南本地生意最低价1VND最高180000000VND
     *                                                  菲律宾本地生意最低价0.01PHP最高400000PHP
     *                                                  新加坡本地生意最低价0.01SGD最高价10000SGP
     *                                                  马来西亚跨境业务（MYR），最低价格为0.01 MRY，最高价格为320000 MYR
     *                                                  越南跨境业务，最低价格为1 VND，最高价格为1000000 VND
     *                                                  菲律宾跨境业务，最低价格为0.01PHP，最高price is 400000PHP
     *                                                  对于新加坡跨境业务，最低价格为0.01SGD，最高价格为400SGD小数点后两位数。
     *      ﹂ product_identifier_code       []    否    这是 GTIN 代码
     *         ﹂  identifier_code           细绳   否    标识符代码 识别码逻辑：
     *                                                      1.必须是数字类型
     *                                                      2.字符数需要满足要求（GTIN：14位，EAN：8/13/14位，UPC：12位，ISBN：13位）
     *                                                      3.不同不允许 SKU 使用相同的 GTIN 代码。
     *         ﹂  identifier_code_type      整数   否    标识符代码类型代码类型值：1-GTIN、2-EAN、3-UPC、4-ISBN（在此字段中输入其中一个）
     *      ﹂ outer_sku_id                  细绳   否    这是外部 sku 标识符
     *
     * @param array $package 包裹信息，包裹重量必填 单位是千克。（最多20个）  小数点后最多两位
     *      ﹂ package_length  整数   否 包裹长度单位是cm   max:60cm    crossboarder业务，包裹尺寸都是必填的，local to local业务，至少需要一个包裹尺寸。
     *      ﹂ package_width   整数    否    包装宽度，单位是cm    max:40cm
     *      ﹂ package_height  整数    否    包裹高度，单位是cm    max:35
     *      ﹂ package_weight  细绳    是    包裹重量，单位是千克。（最多20个）    小数点后最多两位
     *
     * @param bool $is_cod_open 类别规则确定是否需要此参数。如果类别规则为“true”，您可以选择开启或关闭 cod 。如果类别规则为“false”，则不允许打开cod。COD：货到付款(货到付款)
     * @param null $brand_id 否  品牌编号 只能选择已经有资质的品牌，需要保证资质在有效期内
     * @param int|null $warranty_period 否 保修期 需要在平台提供的候选值中选择：
     *      1：“4周”
     *      2：“2个月”
     *      3：“3个月”
     *      4：“4个月”
     *      5：“5个月”
     *      6：“6个月”
     *      7： "7个月"
     *      8:"8个月"
     *      9:"9个月"
     *      10:"10个月"
     *      11:"11个月"
     *      12:"12个月"
     *      13:"2年"
     *      14:"3年"
     *      15:"1周”
     *      16：“2周”
     *      17：“18个月”
     *      18：“4年”
     *      19：“5年”
     *      20：“10年”
     *      21：“终身保修”
     * @param string|null $warranty_policy 否 保修政策 字符长度需要在99以内 建议避免使用中文 因为复制会显示给本地用户
     * @param array $size_chart 尺码表 category_rule 确定是否需要此参数。如果类别规则为“true”，则必须填写，如果类别规则为“false”，则不允许填写
     *      ﹂ img_id    string    是    仅支持输入平台提供的销售属性ID
     * @param array $product_certifications 否 产品认证
     *      ﹂ id          string      是    这是认证id，你可以通过“GetCategoryRule”API获取这个id。
     *      ﹂ images      []          否     图片
     *         ﹂  id      string      是     只能使用UploadImg接口的响应参数作为该请求参数
     *      ﹂ files       []          否     文件
     *         ﹂  id      string      是     您只能使用上传文件接口的请求参数作为请求参数。
     *         ﹂  name    string      是     姓名
     *         ﹂  type    string      是     类型
     * @param string|null $delivery_service_ids 否 可选字段，如果您在送货服务字段中留空，则系统会将此字段设置为“默认”送货选项。
     * @param array $product_attributes 否
     *      ﹂ attribute_id        string    否    属性编号,只支持输入平台提供的商品属性ID（来自GetAttribute API）
     *      ﹂ attribute_values    []     否    属性值
     *         ﹂  value_id        string    否
     *         ﹂  value_name      string    否    值名称此字段供您填写自定义属性值。以下是该字段的一些情况：
     *                                          1. 此字段不能填写普通话。
     *                                          2. 最大字符数为500。
     *                                          3. 该字段支持提交多个值，但这些自定义值不能重复。
     * @param array $exemption_of_identifier_code 否     产品标识代码 (GTIN) 的免税原因
     *      ﹂ exemption_reason     否   这是产品的豁免原因：
     *              在此字段中输入其中一个，类型值：
     *              1-品牌或制造商未提供 GTIN，
     *              2-没有 GTIN 的通用或非品牌产品，
     *              3-产品捆绑包，
     *              4-产品没有 GTIN 的部分
     * @param string $package_dimension_unit 否   此字段仅适用于美国市场（US）（美国必填，非美国地区可选）。而且在其他国家是无效的。
     *              该字段表示商品包装尺寸信息的单位。为可选字段，仅支持单选。
     *              如果您在通过API创建产品时没有填写该字段，系统将默认使用该产品的公制尺寸单位。如果您在该字段中选择了尺寸单位，系统将按照该字段选择的单位保存产品尺寸信息。
     *              值：英制(imperial)、公制(metric) ,
     * @param array $product_video 否
     *      ﹂ video_id     否    产品视频部分。如果您需要在这里上传视频，请按照以下步骤操作：
     *      1. 请通过API[UploadFile] 上传视频文件
     *      2. 请从API[UploadFile] 获取响应信息（video id(file id)）并将ID填入这个值。
     *      3.本视频id只支持1个id字符串。
     * @param string $outer_product_id 否 外部产品标识符
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-08   20:30
     */
    public function CreateProduct(
        string $product_name,
        string $description,
        string $category_id,
        array  $images,
        array  $skus,
        array  $package,
        bool   $is_cod_open = true,
               $brand_id = null,
        int    $warranty_period = null,
        string $warranty_policy = null,
        array  $size_chart = [],
        array  $product_certifications = [],
        string $delivery_service_ids = null,
        array  $product_attributes = [],
        array  $exemption_of_identifier_code = [],
        string $package_dimension_unit = '',
        array  $product_video = [],
        string $outer_product_id = ''

    ) {
        $param           = compact('product_name', 'description', 'category_id', 'is_cod_open', 'skus');
        $param['images'] = collect($images)->map(function ($img) {
            return ['id' => $img];
        })->toArray();
//        $param['skus'] = collect($skus)->map(function ($skus) {
//            $skuData =[
//
//            ];
//            if (!empty($skus['sales_attributes'])) $skuData
//            return $skuData;
//        })->toArray();
        if (!empty($package['package_weight'])) $param['package_weight'] = (string)$package['package_weight'];
        if (!empty($package['package_length'])) $param['package_length'] = $package['package_length'];
        if (!empty($package['package_width'])) $param['package_width'] = $package['package_width'];
        if (!empty($package['package_height'])) $param['package_height'] = $package['package_height'];
        if (!empty($brand_id)) $param['brand_id'] = $brand_id;
        if (!empty($warranty_period)) $param['warranty_period'] = $warranty_period;
        if (!empty($warranty_policy)) $param['warranty_policy'] = $warranty_policy;
        if (!empty($size_chart)) $param['size_chart'] = $size_chart;
        if (!empty($product_certifications)) $param['product_certifications'] = $product_certifications;
        if (!empty($delivery_service_ids)) $param['delivery_service_ids'] = $delivery_service_ids;
        if (!empty($product_attributes)) $param['product_attributes'] = $product_attributes;
        if (!empty($exemption_of_identifier_code)) $param['exemption_of_identifier_code']['exemption_reason'] = $exemption_of_identifier_code;
        if (!empty($package_dimension_unit)) $param['package_dimension_unit'] = $package_dimension_unit;
        if (!empty($product_video)) $param['product_video'] = $product_video;
        if (!empty($outer_product_id)) $param['outer_product_id'] = $outer_product_id;
        $this->setApiParams($param);
        return $this->post('/api/products');
    }

    /**
     * @Notes :停用产品
     * @param $product_ids
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-16   20:14
     */
    public function inactivatedProducts($product_ids)
    {
        $this->addApiParam('product_ids', $product_ids);
        return $this->post('/api/products/inactivated_products');
    }

    /**
     * @Notes : 激活产品
     * @param $product_ids
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-16   20:14
     */
    public function activateProducts($product_ids)
    {
        $this->addApiParam('product_ids', $product_ids);
        return $this->post('/api/products/activate');
    }

    /**
     * @Notes : 恢复已删除的产品
     * 本接口供卖家恢复被删除商品（非冻结商品）。
     * @param $product_ids
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-16   20:15
     */
    public function recoverProducts($product_ids)
    {
        $this->addApiParam('product_ids', $product_ids);
        return $this->post('/api/products/recover');
    }

    /**
     * @Notes : 删除商品
     * @param $product_ids
     * @return array|mixed|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-16   20:23
     */
    public function deleteProducts($product_ids)
    {
        $this->addApiParam('product_ids', $product_ids);
        return $this->delete('/api/products');
    }

    /**
     * @Notes : 获取类别规则
     * @param $category_id
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-16   20:17
     */
    public function getCategoriesRules($category_id)
    {
        $this->addApiParam('category_id', $category_id);
        return $this->get('/api/products/categories/rules');
    }

    /**
     * @Notes : 使用此接口获取推荐类别
     * @param string $product_name 产品名称    细绳    是    标题-xxaa    不允许使用中文字符。    对于美国和英国商店，产品名称必须至少包含 1 个字符且不超过 255 个字符。其他地区店铺，商品名称最少25个字符，最多255个字符。
     * @param string $description 描述    细绳    否    这是一个产品    简要规则：
     * 1.必须符合html语法
     * 2.目前只支持<p> <img> <ul> <li>标签
     * 3.标签不能嵌套
     * 4.只能使用UploadImg接口的请求参数作为请求参数
     * 5. 最多30张图片（请通过UploadImg API转换img链接）。
     * 6.该字段字符限制需要在10000个字符以内。
     * 7、建议避免使用中文，因为复制的不会被识别。
     * @param array $images 只能使用UploadImg API的请求参数作为请求参数最多9张图片，上传顺序中的第一张图片会自动设置为头图
     * id
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-16   20:18
     */
    public function getCategoriesRecommend(string $product_name, $description = '', $images = [])
    {
        $param = [
            'product_name' => $product_name
        ];
        if (!empty($description)) $param['description'] = $description;
        if (!empty($images)) $param['images'] = $images;
        $this->setApiParams($param);
        return $this->post('/api/product/category_recommend');
    }


}
