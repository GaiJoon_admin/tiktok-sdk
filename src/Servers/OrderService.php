<?php

namespace ApiSdk\TikTokShop\Servers;

use Exception;
use GuzzleHttp\Exception\GuzzleException;

class OrderService extends BaseService
{
    /**
     * Notes: 获取订单列表 V2
     * author: lfy
     * Date: 2021/11/19
     * Time: 17:41
     * @throws GuzzleException
     */
    public function getOrderList(array $param = [])
    {
        $createTimeFrom = $param['create_time_from'] ?? 0;                     //订单生成时间的开始时间
        $createTimeTo   = $param['create_time_to'] ?? 0;                       //订单生成时间的截止时间
        $updateTimeFrom = $param['update_time_from'] ?? 0;                     //订单修改时间的开始时间
        $updateTimeTo   = $param['update_time_to'] ?? 0;                       //订单修改时间的截止时间
        $orderStatus    = $param['order_status'] ?? 0;                         //订单状态:UNPAID = 100;AWAITING_SHIPMENT = 111; AWAITING_COLLECTION = 112;IN_TRANSIT = 121;DELIVERED = 122;COMPLETED = 130;CANCELLED = 140;
        $cursor         = $param['cursor'] ?? '';                              //游标内容 第一次传空串，之后传上一次的cursor返回值

        $pageSize = $param['pageSize'] ?? 50;                            //每页请求数量 最多一页50条
        $sortBy   = $param['sortBy'] ?? 'CREATE_TIME';                   //使用此字段可以获取按特定字段排序的订单。取值范围:CREATE_TIME、UPDATE_TIME。默认值:CREATE_TIME
        $sort     = $param['sortType'] ?? 1;                             //排序方式：1 (DESC)、2 (ASC)缺省值:1
        $this->addApiParam('page_size', $pageSize);
        if (!empty($sort)) $this->addApiParam('sort_type', $sort);
        if (!empty($sortBy)) $this->addApiParam('sort_by', $sortBy);
        if (!empty($createTimeFrom)) $this->addApiParam('create_time_from', $createTimeFrom);
        if (!empty($createTimeTo)) $this->addApiParam('create_time_to', $createTimeTo);
        if (!empty($updateTimeFrom)) $this->addApiParam('update_time_from', $updateTimeFrom);
        if (!empty($updateTimeTo)) $this->addApiParam('update_time_to', $updateTimeTo);
        if (!empty($orderStatus)) $this->addApiParam('order_status', $orderStatus);
        if (!empty($cursor)) $this->addApiParam('cursor', $cursor);
        return $this->post('/api/orders/search');
    }

    /**
     * Notes: 订单详情
     * author: lfy
     * Date: 2021/11/19
     * Time: 17:48
     * @param $order
     * @return array|bool|void
     * @throws GuzzleException
     */
    public function getOrderDetail($order)
    {
        $this->addApiParam('order_id_list', $order);//订单id：建议小于20，必须小于50
        return $this->post('/api/orders/detail/query');
    }

    /**
     * Notes: 订单发货
     * author: lfy
     * Date: 2021/11/19
     * Time: 18:02
     * @param $orderId //订单编号
     * @param $trackingNumber //运单号
     * @param $shippingProviderId //
     * @param string $selfShipment //
     * @param string $pickUp //
     * @return array|bool|void
     * @throws GuzzleException
     */
    public function shipOrder($orderId, $trackingNumber, $shippingProviderId, string $selfShipment = '', string $pickUp = '')
    {
        $this->addApiParam('order_id', $orderId);
        $this->addApiParam('tracking_number', $trackingNumber);
        $this->addApiParam('shipping_provider_id', $shippingProviderId);
        if (!empty($selfShipment)) $this->addApiParam('self_shipment', $selfShipment);
        if (!empty($pickUp)) $this->addApiParam('pick_up', $pickUp);
        return $this->post('/api/order/rts');
    }


    /********************************************************** 202309 版本********************************************/

    /**
     *  获取订单列表
     * @param $param
     * @return array|bool|null
     * @throws GuzzleException
     */
    public function getOrderListV23($param)
    {
        $this->addHeaderParam('content-type','application/json');
        $createTimeFrom = $param['create_time_ge'] ?? 0;                        //订单生成时间的开始时间
        $createTimeTo   = $param['create_time_lt'] ?? 0;                        //订单生成时间的截止时间
        $updateTimeFrom = $param['update_time_ge'] ?? 0;                        //订单修改时间的开始时间
        $updateTimeTo   = $param['update_time_lt'] ?? 0;                        //订单修改时间的截止时间
        $orderStatus    = $param['order_status'] ?? '';                         //订单状态:UNPAID;AWAITING_SHIPMENT; AWAITING_COLLECTION;IN_TRANSIT;DELIVERED;COMPLETED;CANCELLED;
        if (!empty($createTimeFrom)) $this->addApiParam('create_time_ge', $createTimeFrom);
        if (!empty($createTimeTo)) $this->addApiParam('create_time_lt', $createTimeTo);
        if (!empty($updateTimeFrom)) $this->addApiParam('update_time_ge', $updateTimeFrom);
        if (!empty($updateTimeTo)) $this->addApiParam('update_time_lt', $updateTimeTo);
        if (!empty($orderStatus)) $this->addApiParam('order_status', $orderStatus);

        $pageSize = $param['pageSize'] ?? 100;                            //每页请求数量 最多一页100条
        $sortBy   = $param['sortBy'] ?? 'create_time';                    //使用此字段可以获取按特定字段排序的订单。取值范围:create_time, update_time。默认值:CREATE_TIME
        $sort     = $param['sortType'] ?? 1;                              //排序方式：1 (DESC)、2 (ASC)缺省值:1ASC;DESC
        $this->addCommonParams('page_size', $pageSize);
        if (!empty($sort)) $this->addCommonParams('sort_order', $sort == 2 ? "ASC" : 'DESC');
        if (!empty($sortBy)) $this->addCommonParams('sort_field', $sortBy);
        $cursor = $param['cursor'] ?? '';                              //游标内容 第一次传空串，之后传上一次的cursor返回值
        if (!empty($cursor)) $this->addCommonParams('page_token', $cursor);
        return $this->post('/order/202309/orders/search');

    }

    /**
     * 获取订单详情
     * @param array $order
     * @return array|bool|null
     * @throws GuzzleException
     */
    public function orderDetail(string $order)
    {
        $this->addHeaderParam('content-type','application/json');
        $this->addApiParam('ids', $order);//订单id：建议小于20，必须小于50
        return $this->get('/order/202309/orders');
    }

    public function shipOrderV23(){

    }
}
