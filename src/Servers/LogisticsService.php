<?php

namespace ApiSdk\TikTokShop\Servers;

use Exception;
use GuzzleHttp\Exception\GuzzleException;

class LogisticsService extends BaseService
{
    /**
     * @Notes : 获取运单信息
     * @param $orderId
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-04-20   17:04
     */
    public function getShippingInfo($orderId)
    {
        $this->addApiParam('order_id', $orderId);//订单id
        return $this->get('/api/logistics/ship/get');
    }

    /**
     * @Notes : 更新运单信息
     * @param $orderId
     * @param $trackingNumber
     * @param $providerId
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-04-20   18:13
     */
    public function updateShippingInfo($orderId, $trackingNumber, $providerId)
    {
        $this->addApiParam('order_id', $orderId);//订单id
        $this->addApiParam('tracking_number', $trackingNumber);
        $this->addApiParam('provider_id', $providerId);
        return $this->post('/api/logistics/tracking');

    }

    /**
     * @Notes : 获取面单
     * @param $orderId
     * @param string $documentType //使用此字段指定要获取的文档类型: SHIPPING_LABEL/ PICK_LIST/ SL_PL;SL_PL 是在一个 pdf 中同时打印 SHIPPING_LABEL 和 PICK_LIST。如果要打印运输标签和提货清单，开发人员应分别选择这两个值并调用此 API 两次。
     * @param string $documentSize
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-04-20   17:25
     */
    public function getShippingDocument($orderId, string $documentType = "SHIPPING_LABEL", string $documentSize = 'A5')
    {
        $this->addApiParam('order_id', $orderId);//订单id
        $this->addApiParam('document_type', 'SL_PL');
        $this->addApiParam('document_size', 'A6');
        return $this->get('/api/logistics/shipping_document');
    }


    /**
     * @Notes : 根据运单号获取面单(包含合并)
     * author: YZY
     * date: 2023/9/26 9:21
     * @param $shop_id
     * @param $package_id
     * @param int $documentType (可用值：SHIPPING_LABEL/ PICK_LIST/ PACK_LIST ,- SHIPPING_LABEL = 1 ,  - PICK_LIST = 2,  - SL+PL = 3, - SHIPPING_LABEL_PICTURE = 4)
     * @param int $documentSize (使用此字段指定要获取的文档的大小。可用值：A6/A5。默认为 A6。 - A6 = 0, - A5 = 1)
     * @return array|bool|void
     * @throws GuzzleException
     */
    public function fulfillmentShippingDocument($shop_id, $package_id, $documentType = 1, $documentSize = 1)
    {
        $this->addApiParam('shop_id', $shop_id);
        $this->addApiParam('package_id', $package_id);
        $this->addApiParam('document_type', $documentType);
        $this->addApiParam('document_size', $documentSize);
        return $this->get('/api/fulfillment/shipping_document');
    }

    /**
     * 根据包裹信息获取合单列表
     * author: YZY
     * date: 2023/10/6 10:28
     * @param $shop_id
     * @param $package_id
     * @return array|bool|void
     * @throws GuzzleException
     */
    public function fulfillmentDetail($shop_id, $package_id)
    {
        $this->addApiParam('shop_id', $shop_id);
        $this->addApiParam('package_id', $package_id);
        return $this->get('/api/fulfillment/detail');
    }


    /**
     * @Notes : 获取仓库列表
     * @param string $addressType //SALES_WAREHOUSE/ RETURN_WAREHOUSE/ LOCAL_RETURN_WAREHOUSE.
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-04-20   18:14
     */
    public function getAddressList(string $addressType = '')
    {
        if (!empty($addressType)) $this->addApiParam('address_type', $addressType);
        return $this->get('/api/logistics/addresses');
    }

    /**
     * @Notes :获取仓库列表
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-13   17:23
     */
    public function getWarehouseList()
    {

        return $this->get('/api/logistics/get_warehouse_list');
    }

    /**
     * @Notes : 运送包裹
     * @param $package_id
     * @param $shipping_provider_id
     * @param $tracking_number
     * @param $pick_up_type
     * @param $pick_up_end_time
     * @param $pick_up_start_time
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-14   13:57
     */
    public function fulfillmentRts(
        $package_id,
        $shipping_provider_id = null,
        $tracking_number = null,
        $pick_up_type = null,
        $pick_up_end_time = null,
        $pick_up_start_time = null
    )
    {
        $param = compact('package_id');
        if (!empty($shipping_provider_id)) $param['self_shipment']['shipping_provider_id'] = $shipping_provider_id;
        if (!empty($tracking_number)) $param['self_shipment']['tracking_number'] = $tracking_number;
        if (!empty($pick_up_type)) $param['pick_up_type'] = $pick_up_type;
        if (!empty($pick_up_end_time)) $param['pick_up']['pick_up_end_time'] = $pick_up_end_time;
        if (!empty($pick_up_start_time)) $param['pick_up']['pick_up_start_time'] = $pick_up_start_time;
        $this->setApiParams($param);
        return $this->post('/api/fulfillment/rts');

    }

    /********************************************************** 202309 版本********************************************/

    /**
     * 运送包裹
     * @param $package_id
     * @param $shipping_provider_id
     * @param $tracking_number
     * @param $pick_up_end_time
     * @param $pick_up_start_time
     * @param $handover_method
     * @return array|bool|null
     * @throws GuzzleException
     */
    public function ShipPackage(
        $package_id,
        $shipping_provider_id = null,
        $tracking_number = null,
        $pick_up_end_time = null,
        $pick_up_start_time = null,
        $handover_method = null
    )
    {
        $this->addHeaderParam('content-type','application/json');
        $param = [];
        if (!empty($shipping_provider_id)) $param['self_shipment']['shipping_provider_id'] = $shipping_provider_id;
        if (!empty($tracking_number)) $param['self_shipment']['tracking_number'] = $tracking_number;
        if (!empty($handover_method)) $param['handover_method'] = $handover_method;
        if (!empty($pick_up_end_time)) $param['pickup_slot']['end_time'] = $pick_up_end_time;
        if (!empty($pick_up_start_time)) $param['pickup_slot']['start_time'] = $pick_up_start_time;
        if (!empty($param)) $this->setApiParams($param);
        return $this->post("/fulfillment/202309/packages/{$package_id}/ship");
    }

    /**
     * @Notes :获取仓库列表
     * @return array|bool|void
     * @throws GuzzleException
     * @author : Lfy
     * @Time : 2022-12-13   17:23
     */
    public function warehouseList()
    {
        $this->addHeaderParam('content-type','application/json');
        return $this->get('/logistics/202309/warehouses');
    }

    /**
     * @param $orderId
     * @return array|bool|null
     * @throws GuzzleException
     */
    public function GetTracking($orderId){
        return $this->get("/fulfillment/202309/orders/${orderId}/tracking");
    }

    /**
     * @param $delivery_option_id
     * @return array|bool|null
     * @throws GuzzleException
     */
    public function GetShippingProviders($delivery_option_id){
        return $this->get("/logistics/202309/delivery_options/${delivery_option_id}/shipping_providers");
    }
}
