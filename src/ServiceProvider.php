<?php

namespace ApiSdk\TikTokShop;

use ApiSdk\TikTokShop\Facade\TikTokPackage;
use Illuminate\Support\ServiceProvider as Service;

class ServiceProvider extends Service
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {


        //这里使用到了facades中的字符串
        $this->app->singleton('TikTok',function(){
            return new TikTokPackage();

            //我们可以通过facades的aliase访问下面的MoreAction
            //会在config的app.php文件中进行服务提供者和别名的注册
//            return $this->app->make('ArcherWong\LaraPackage\MoreAction');
        });
        $this->mergeConfigFrom(
            __DIR__.'/config/logging.php', 'logging'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
                             __DIR__ . '/config/TikTok.php' => config_path('platform/TikTok.php'), // 发布配置文件到 laravel 的config 下
                         ]);
    }
}
