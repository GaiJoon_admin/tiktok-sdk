<?php
return [
    'base_url'              => env('TIKTOK_BASE_URL', 'https://open-api.tiktokglobalshop.com'),
    'sandbox_base_url'      => env('TIKTOK_SANDBOX_BASE_URL', 'https://open-api.tiktokglobalshop.com'),
    'auth_base_url'         => env('TIKTOK_AUTH_BASE_URL', 'https://auth.tiktok-shops.com'),//授权请求默认域
    'auth_sandbox_base_url' => env('TIKTOK_AUTH_SANDBOX__BASE_URL', 'https://auth.tiktok-shops.com'),//授权请求默认域
    'sandbox_type'          => env('TIKTOK_SANDBOX_TYPE', 0),
    'appKey'                => env('TIKTOK_APP_KEY', ''),
    'appSecret'             => env('TIKTOK_APP_SECRET', ''),

    'appKeyUs'                => env('TIKTOK_US_APP_KEY', ''),
    'appSecretUs'             => env('TIKTOK_US_APP_SECRET', ''),
    'base_us_url'              => env('TIKTOK_US_BASE_URL', 'https://open-api.tiktokglobalshop.com'),
    'auth_base_us_url'         => env('TIKTOK_US_AUTH_BASE_URL', 'https://auth.tiktok-shops.com'),//授权请求默认域

    'maxRetryTimes'         => env('TIKTOK_API_RETRY_TIMES', 1),  //  最大重试次数 重试 最多重试10次
    'retryInterval'         => env('TIKTOK_API_RETRY_INTERVAL', 1), // 重试间隔 单位 秒(s)
    'order_status'          => [
        //0   => 'UNKNOWN',//未知状态
        100 => 'UNPAID',//待付款
        111 => 'AWAITING_SHIPMENT',//等待发货
        112 => 'AWAITING_COLLECTION',//等待揽收
        121 => 'IN_TRANSIT',//在途
        122 => 'DELIVERED',//已交付
        130 => 'COMPLETED',//完成
        140 => 'CANCELLED',//取消
    ],
    'ext_status'            => [
        0   => 'UN_DEFINED',
        101 => 'RC_PROCESSING ',//风控进行中
        102 => 'RC_APPROVED ',//风控获批
        103 => 'RC_AUTO_APPROVED',//风控过期，无回调结果
        104 => 'RC_FAILED',//风控失败
        201 => 'CANCEL_PENDING ',
        202 => 'CANCEL_REJECT ',
        203 => 'CANCEL_COMPLETED ',
    ],
    'sku_ext_status'        => [
        201 => 'CANCEL_PENDING',
        202 => 'CANCEL_REJECT',
        203 => 'CANCEL_COMPLETED',
    ],
    'payment_method'        => [
        'BANK_TRANSFER'        => 1,
        'CASH'                 => 2,
        'DANA_WALLET'          => 3,
        'BANK_CARD'            => 4,
        'OVO'                  => 5,
        'CASH_ON_DELIVERY'     => 6,
        'GO_PAY'               => 7,
        'PAYPAL'               => 8,
        'APPLEPAY'             => 9,
        'SHOPEEPAY'            => 10,
        'KLARNA'               => 11,
        'KLARNA_PAY_NOW'       => 12,
        'KLARNA_PAY_LATER'     => 13,
        'KLARNA_PAY_OVER_TIME' => 14,
        'TRUE_MONEY'           => 15,
        'RABBIT_LINE_PAY'      => 16,
        'IBANKING'             => 17,
        'TOUCH_GO'             => 18,
        'BOOST'                => 19,
        'ZALO_PAY'             => 20,
        'MOMO'                 => 21,
        'BLIK'                 => 22,
    ],
    'sku_display_status'    => [
        100 => 'UNPAID',
        110 => 'TO_SHIP',
        111 => 'AWAITING_SHIPMENT',
        112 => 'AWAITING_COLLECTION',
        121 => 'IN_TRANSIT',
        122 => 'DELIVERED',
        130 => 'COMPLETED',
        140 => 'CALCELLED',
    ]

];
