<?php
namespace ApiSdk\TikTokShop\Facade;
use Illuminate\Support\Facades\Facade;
class TikTokPackage extends Facade
{
    public function execute()
    {
        return 'execute';
    }
    public static function getFacadeAccessor()
    {
        //return 的字符串会在相应的provider中使用
        return 'tiktok_sdk';
    }
}
